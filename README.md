# [Build a Personal Portfolio Webpage](https://learn.freecodecamp.org/responsive-web-design/responsive-web-design-projects/build-a-personal-portfolio-webpage)
## [Free Code Camp](https://www.freecodecamp.org) - Responsive Web Design Projects

## Objective
Build a personal portfolio webpage that is functionally similar to the [example page](https://codepen.io/freeCodeCamp/full/zNBOYG).

The [completed page](https://elrolfe-fcc-rwd.gitlab.io/portfolio) was coded with HTML and CSS.

## User Stories
The personal portfolio page fulfills the following user stories provided by FreeCodeCamp.org:
1. My portfolio should have a welcome section with an id of `welcome-section`.
2. The welcome section should have an `h1` element that contains text.
3. My portfolio should have a projects section with an id of `projects`.
4. The projects section should contain at least one element with a class of `project-title` to hold a project.
5. The projects section should contain at least one link to a project.
6. My portfolio should have a navbar with an id of `navbar`.
7. The navbar should contain at least one link that I can click on to navigate to different sections of the page.
8. My portfolio should have a link with an id of `profile-link`, which opens my GitHub or FCC profile in a new tab.
9. My portfolio should have at least on media query.
10. The height of the welcome section should be equal to the height of the viewport.
11. The navbar should always be at the top of the viewport.